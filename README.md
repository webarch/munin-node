# Munin Node

**THIS REPO IS NO LOGNER BEING UPDATED, SEE [git.coop/webarch/munin](https://git.coop/webarch/munin).**

Ansible role for configuring a Debian or Ubuntu server as a munin Node (client).

See [the defaults file](defaults/main.yml) for the settings.

Assuming you have a YAML `hosts.yml` file then you should add entries something like this to it:

```yaml
---
all:
  vars:
    munin_servers: "{{ groups['munin_servers'] }}"
  children:
    buster_servers:
      hosts:
        example.org:
          vars:
            munin_plugins_disabled:
              - entropy
              - name: multips
              - name: multips_memory
            munin_plugins_enabled:
              - name: cpu
                dir: /usr/lib/munin-c/plugins
                file: munin-plugins-c
              - name: mysql_commands
                file: mysql_
              - name: mysql_connections
                file: mysql_
        example.org.uk:
          vars:
            munin_multips_processes: apache2 php mysqld
            munin_multips_memory_processes: apache2 php-fpm7.3 mysqld
            munin_plugins_enabled:
              - name: multips
              - name: multips_memory
              - name: phpfpm_multi_average
                dir: /usr/local/share/munin/plugins
              - name: phpfpm_multi_connections
                dir: /usr/local/share/munin/plugins
              - name: phpfpm_multi_memory
                dir: /usr/local/share/munin/plugins
              - name: phpfpm_multi_processes
                dir: /usr/local/share/munin/plugins
    munin_servers:
      hosts:
        monitor.webarch.net:
          ansible_host: 81.95.52.37
...
```

In order to use this role.

Some inspiration for this role was taken from
[systemli](https://github.com/systemli/ansible-role-munin-node) and
[geerlingguy](https://github.com/geerlingguy/ansible-role-munin-node) roles.
